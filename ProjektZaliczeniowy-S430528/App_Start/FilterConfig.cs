﻿using System.Web;
using System.Web.Mvc;

namespace ProjektZaliczeniowy_S430528
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
