namespace ProjektZaliczeniowy_S430528.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Films
    {
        public int FilmsId { get; set; }
        [Required(ErrorMessage = "Pole Title jest wymagane.")]
        [StringLength(20)]
        public string Title { get; set; }
        [Required(ErrorMessage = "Pole MovieLength jest wymagane.")]
        [StringLength(20)]
        public string MovieLength { get; set; }
        [Required(ErrorMessage = "Pole MovieCategory jest wymagane.")]
        [StringLength(20)]
        public string MovieCategory { get; set; }
        [Required(ErrorMessage = "Pole Director jest wymagane.")]
        [StringLength(20)]
        public string Director { get; set; }
        [Required(ErrorMessage = "Pole ReleaseDate jest wymagane.")]
        public System.DateTime ReleaseDate { get; set; }
    }
}
